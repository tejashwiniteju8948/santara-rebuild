<?php

return [
    "API_ADMIN_VERSION" => "v3.7.1/",
    "API_CLIENT_VERSION" => "v3.7.1",
    "BASE_API_CLIENT_URL" => "https://tulabi.com:3801",
    "BASE_API_ADMIN_URL" => "https://tulabi.com:3701/",
    "BASE_FILE_URL" => "https://dev.santara.id",
    "BASE_API_FILE" => "https://dev.santara.id",
    "STORAGE_GOOGLE" => "https://storage.googleapis.com/asset-santara-staging/santara.co.id/",
    "STORAGE_GOOGLE_BUCKET" => "asset-santara-staging",
    "STORAGE_BUCKET" => "https://storage.googleapis.com/asset-bucket-staging/"
];